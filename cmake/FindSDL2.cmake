message ("-- SDL2")
FIND_PATH(SDL2_INCLUDE_DIR SDL.h
    ${CROSS_PLATFORM_PATHS}/SDL2
    /usr/include
    /usr/local/include
    /sw/include
    /opt/local/include
    $ENV{PROGRAMFILES}/SDL2
    CACHE STRING "Where to find SDL.h"
    )

if(NOT APPLE)
    find_package(Threads)
endif()

if(WIN32)
    set(SDL2_LIBRARY
        ${PLATFORM_PATHS}/SDL2/lib/${PLATFORM_ARCH}/SDL2main.lib
        ${PLATFORM_PATHS}/SDL2/lib/${PLATFORM_ARCH}/SDL2.lib
        ${PLATFORM_PATHS}/SDL2/lib/${PLATFORM_ARCH}/SDL2_image.${LIBRARY_EXTENSION}
        ${PLATFORM_PATHS}/SDL2/lib/${PLATFORM_ARCH}/SDL2_mixer.${LIBRARY_EXTENSION}
        ${PLATFORM_PATHS}/SDL2/lib/${PLATFORM_ARCH}/SDL2_ttf.${LIBRARY_EXTENSION}
        ${CMAKE_THREAD_LIBS_INIT}
        CACHE STRING "All the SDL2 Libraries"
        )
elseif(UNIX)
    set(SDL2_LIBRARY
        ${PLATFORM_PATHS}/SDL2/lib/libSDL2main.a
        ${PLATFORM_PATHS}/SDL2/lib/libSDL2.${LIBRARY_EXTENSION}
        ${PLATFORM_PATHS}/SDL2/lib/libSDL2_image.${LIBRARY_EXTENSION}
        ${PLATFORM_PATHS}/SDL2/lib/libSDL2_mixer.${LIBRARY_EXTENSION}
        ${PLATFORM_PATHS}/SDL2/lib/libSDL2_ttf.${LIBRARY_EXTENSION}
        ${CMAKE_THREAD_LIBS_INIT}
        CACHE STRING "All the SDL2 Libraries"
        )
endif(WIN32)

IF(SDL2_INCLUDE_DIR) 
SET(SDL2_FOUND "YES") 
MESSAGE(STATUS "Found SDL2.")
ENDIF(SDL2_INCLUDE_DIR)

if (SDL2_INCLUDE_DIR)
    SET(SDL2_FOUND "YES")
    MESSAGE(STATUS "Found SDL2.")
    MESSAGE(${SDL2_INCLUDE_DIR})
endif()

set(PACKAGE_VERSION "2.0.8")

if(PACKAGE_VERSION VERSION_LESS PACKAGE_FIND_VERSION)
  set(PACKAGE_VERSION_COMPATIBLE FALSE)
else()
  set(PACKAGE_VERSION_COMPATIBLE TRUE)
  if(PACKAGE_FIND_VERSION STREQUAL PACKAGE_VERSION)
    set(PACKAGE_VERSION_EXACT TRUE)
  endif()
endif()


# if the installed or the using project don't have CMAKE_SIZEOF_VOID_P set, ignore it:
if("${CMAKE_SIZEOF_VOID_P}" STREQUAL "" OR "8" STREQUAL "")
return()
endif()

# check that the installed version has the same 32/64bit-ness as the one which is currently searching:
if(NOT CMAKE_SIZEOF_VOID_P STREQUAL "8")
math(EXPR installedBits "8 * 8")
set(PACKAGE_VERSION "${PACKAGE_VERSION} (${installedBits}bit)")
set(PACKAGE_VERSION_UNSUITABLE TRUE)
endif()
